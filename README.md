# Lenguajes de programación segun la proximidad al nucleo de la maquina

En cada página se trata un **nivel** distinto de proximidad al nucleo:

### [`Lenguajes de bajo nivel`](p1.md)

### [`Lenguajes de medio nivel`](p3.md)

### [`Lenguajes de alto nivel`](p4.md)

